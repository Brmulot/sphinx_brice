# SPHINX_Brice



## Prérequis 

### Installation pour le html :
    - `pip install sphinx` 
    - `make html`

### installation pour le pdf :
    - [Miketex](https://miktex.org/download)
    - [Strawbery perl](https://strawberryperl.com/)
    - `make latexpdf`

si vous avez le message d'erreur : 
`'latexmk' n’est pas reconnu en tant que commande interne
ou externe, un programme exécutable ou un fichier de commandes.`

Dans anaconda prompt faire :
    - `mpm --install (mise a jour des packages de miktex)`
    - `make latexpdf`

**vous devez être dans le dossier docs avant d'effectuer les `make`**

## Les fichiers à ouvrir :

Le dossier index.html se trouve dans docs -> build -> html
Le dossier vivelescollegues se trouve dans docs -> build -> latex
