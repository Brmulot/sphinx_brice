.. Lumache documentation master file, created by
   sphinx-quickstart on Thu Jan  5 10:28:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation de Vive les collègues !
======================================================

**Vive les collègues** : Le principe de ce projet va être de faire en sorte que
chaque nouveau collègue qui entre dans l'entreprise soit ajouté à un fichier qui
les recense tous. Pour plus d'information allez checker le brief sur `Simplonline
<https://simplonline.co/briefs/ac69f0aa-ce54-435e-8cd5-92e0826911ad>`_ vous y
trouverez toute la documentation que j'ai pu utiliser afin d'avoir des informations
supplémentaire.

Cliquez pour voir les étapes :ref:`d'installation <installation>` du projet.

Vous pouvez regarder la section :doc:`usage` pour voir comment fonctionne les
fonctions qui y sont définie !

.. note::
    
    Pour tout ce qui est de l'usage des fonctions et des merge request veuillez vous
    référer au `wiki <https://gitlab.com/simplonclermontia3/vivelescollegues_brice/-/wikis/Procedure>`_ et 
    au `readme <https://gitlab.com/simplonclermontia3/vivelescollegues_brice/-/blob/main/README.md>`_ 
    présent sur le GitLab

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Contents
--------

.. toctree::

   usage