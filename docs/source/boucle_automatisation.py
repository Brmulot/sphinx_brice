import pandas as pd
import numpy as np
import inquirer

from liste_metier import metiers
from datetime import datetime
#Convention : Nom, Prénom, Date embauche, Poste


#fonction case du nom 
import pandas as pd
import numpy as np
import inquirer

from liste_metier import metiers
from datetime import datetime

#Convention : Nom, Prénom, Date embauche, Poste


def surname():
    """
    Demande le nom de l'employé à l'utilisateur.

    Retourne
    --------
    str
        Le nom de l'employé.

    Exemple
    -------
    >>> surname()
    Entrez le nom du nouvel employé :
    """
    a = input("Entrez le nom du nouvel employé : ")
    return a


def name():
    """
    Demande le prénom de l'employé à l'utilisateur.

    Retourne
    --------
    str
        Le prénom de l'employé.
    """
    b = input("Entrez le prénom du nouvel employé : ")
    return b.title()


def time():
    """
    Demande la date d'embauche de l'employé à l'utilisateur.

    Retourne
    --------
    str
        La date d'embauche de l'employé.
    """
    j = input("Entrez le jour du mois d'arrivée : ")
    m = input ("Etrez le mois d'arrivée : ")
    y = input ("entrée l'année d'arrivée : ")
    date = print(f"{j}/ {m} /{y}")
                 
    return date


def position(choix_metier):
    """
    Demande la position de l'employé à l'utilisateur.

    Paramètres
    ----------
    choix_metier : list
        Liste des positions possibles.

    Retourne
    --------
    str
        La position choisie par l'utilisateur.
    """
    questions = [inquirer.List('position', message = "Quel position souhaitez-vous ?",
                                choices = choix_metier,),]
    answers = inquirer.prompt(questions)

    return answers['position']


def prescripteur():
    """
    Demande le nom du prescripteur à l'utilisateur.

    Retourne
    --------
    str
        Le nom du prescripteur.
    """
    e = input("Entrez votre nom : ")
    return e

def ajout_auto(data):
    """
    Ajoute un employé à la liste `data`.

    Paramètres
    ----------
    data : pandas.DataFrame
        Tableau de données des employés.

    Retourne
    --------
    pandas.Series
        Ligne ajoutée au tableau de données.
    """
    res = data.loc[len(data)]=[nom, prenom, date, poste, prescri, datetime.now()]
    return res
   


if __name__ == "__main__":

    df = pd.read_csv(r'C:\Users\utilisateur\Documents\vivelescollegues_brice\liste_employes.csv', index_col=0)
    df['Prescripteur'] = np.nan
    df['Date_ajout'] = np.nan

    choix_metier = metiers()
    nom = surname().upper()
    prenom = name()
    poste = position(choix_metier)
    print(poste)
    prescri = prescripteur()

    print(df)
    ajout_auto(df)
    print(df)

    df.to_csv('liste_employes.csv', sep=",")