Usage
=====

.. _installation:

Installation
------------

Pour utiliser le fichier veuillez cloner mon dépôt `GitLab
<https://gitlab.com/simplonclermontia3/vivelescollegues_brice>`_ 

*Sur Gitbash rentrer la commande suivante :*

.. code-block:: console

   $ git clone https://gitlab.com/simplonclermontia3/vivelescollegues_brice 

Ainsi vous obtiendrez un *fichier* du nom de **vivelescollegues_brice** contenant l'intégralité du code !

Appeler le fichier dans un autre dossier
----------------------------------------

Si vous avez besoin d'utiliser ces fonctions dans un nouveau fichier voici comment faire :

>>> import votre_chemin/boucle_automatisation
>>> boucle_automatisation.surname()
Entrez le nom du nouvel employé : 

Si votre fichier `.py` ce trouve dans le même fichier que `boucle_automatisation.py` faites simplement :

>>> import boucle_automatisation

Les différentes fonctions présentes
-----------------------------------

.. automodule:: boucle_automatisation
   :members:
   :undoc-members:
   :show-inheritance:
